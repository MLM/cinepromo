#!/usr/bin/python
# -*- coding: utf-8 -*-
""" transfert de bandes annonces """

import json
from xml.etree import ElementTree
import os.path
import argparse
from urllib.request import urlretrieve
import requests
import argcomplete

def debug(args, message):
    """ debug """
    if args.debug:
        print(message)

def parser_cli():
    """ Partie cli """
    parser = argparse.ArgumentParser(description="tba\n"
                                     +"transfert de bandes annonces"
                                     +"launch: eval \"$(register-python-argcomplete3 tba.py)\"\n"
                                     +" for auto completion\n")
    parser.add_argument('--url', '-u', dest='url',
                        default='https://staubincormierlemauclerc.cineoffice.fr/TMSexport/staubincormierlemauclerc',
                        help='Url du fichier xml')
    parser.add_argument('--dest', '-r', dest='dest', default='/tmp/',
                        help='repertoire de destination des bande annonces')
    parser.add_argument('--debug', '-d', dest='debug', action='store_true',
                        help='Debug mode')

    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    debug(args, args)
    return args

def getlist(args):
    """ GetList """
    rfilms = requests.get(url=args.url, stream=True)
    rfilms.raw.decode_content = True
    events = ElementTree.iterparse(rfilms.raw)
    try:
        for event, elem in events:
#            debug(args,elem.tag)
#            debug(args, elem.text)
            if elem.tag == 'FilmTitle':
                FilmTitle = elem.text
            if elem.tag == 'FilmAllocineId':
                FilmAllocineId = elem.text
            if elem.tag == 'FilmTrailer' and elem.text is not None and FilmAllocineId is not None:
                debug(args, 'Film: '+str(FilmTitle)+' id:'+str(FilmAllocineId)+' url:'
                      +str(elem.text))
                if os.path.isfile(args.dest+'/'+FilmAllocineId+'.mp4'):
                    debug(args, "File exist")
                else:
                    urlretrieve(elem.text, args.dest+'/'+FilmAllocineId+'.mp4')
                FilmAllocineId = ''
    except Exception as error:
        print("erreur fichier source: " + json.dumps(error))

# ///////////////////////////////////////////// Main ///////////////////////////////////////////// #
def main():
    """ Main """
    args = parser_cli()
    getlist(args)

if __name__ == "__main__":
    main()

exit()
