#!/usr/bin/python3
# -*- coding:utf-8 -*-
####################################################################################
# File name :web.py
# Version : 0.2
# Purpose : Affichage Promotion cinema Mauclers St Aubin du Cormier
# Usages : see README
# Date : 07/11/2015
# Author : mikael.lemasson@orange.fr
# Python : 2.7
# License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
# How To Run : python web.py
#####################################################################################
import os
import sys
import io
import json
import syslog
import time
from flask import Flask, request, render_template, send_file

app = Flask(__name__)

JSON = 'config.json'


@app.route('/', methods=['GET', 'POST'])
def index():
	filedatabase = open(JSON)
	database = json.load(filedatabase)
	if request.method == 'POST':
#		syslog.syslog("a:"+json.dumps(request.form))
		database['PlaceDispo']['duree'] = request.form['duree']
		database['PlaceDispo']['url'] = request.form['url']
		database['PlaceDispo']['nbsalle'] = request.form['nbsalle']
		database['PlaceDispo']['token'] = request.form['token']
		database['PlaceDispo']['ratioba'] = request.form['ratioba']
		database['PlaceDispo']['urlba'] = request.form['urlba']
		with open(JSON, 'w') as outfile:
			json.dump(database, outfile, sort_keys = True, indent = 4,
			ensure_ascii=False)

	templateData = {'duree': database['PlaceDispo']['duree'],'nbsalle':database['PlaceDispo']['nbsalle'],'url':database['PlaceDispo']['url'],'token':database['PlaceDispo']['token'],'ratioba':database['PlaceDispo']['ratioba'],'urlba':database['PlaceDispo']['urlba']}
	return render_template('config.html',**templateData)

@app.route('/grid')
def p_grid():
	return send_file(JSON)

@app.route('/server', methods=['GET', 'POST'])
def server():
	if request.method == 'POST':
		filedatabase = open(JSON)
		database = json.load(filedatabase)
		if request.form['oper'] == 'add':
			rows = database['rows']
			id = 0
			for pub in rows:
				id = int(pub['id'])
			id += 1
			newarticle = {'id': id,'type': str(request.form['type']), 'url': str(request.form['url']), 'duree': str(request.form['duree']), 'active': str(request.form['active'])}
			database['rows'].append(newarticle)

		elif request.form['oper'] == 'edit':
			newarticle = {'id': str(request.form['id']),'type': str(request.form['type']), 'url': str(request.form['url']), 'duree': str(request.form['duree']), 'active': str(request.form['active'])}
			rows = database['rows']
			for pub in rows:
				if str(pub['id']) == str(request.form['id']) :
					database['rows'].remove(pub)
					database['rows'].insert(int(pub['id'])-1,newarticle)

		elif request.form['oper'] == 'del':
			rows = database['rows']
			for pub in rows:
				if str(pub['id']) == str(request.form['id']) :
					syslog.syslog(str(pub['id']))
					database['rows'].remove(pub)

		else:
			return "<html><body>Erreur API oper inconnu</body></html>"

#		syslog.syslog("a:"+json.dumps(database['rows']))
		with open(JSON, 'w') as outfile:
			json.dump(database, outfile, sort_keys = True, indent = 4,
			ensure_ascii=False)
		return "<html><body>"+json.dumps(database)+"</body></html>"
	else:
		return "<html><body>Erreur API</body></html>"

if __name__ == '__main__':
      app.run (host='0.0.0.0',port=8000,debug=True)
