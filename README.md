# CinePromo

Affichage Promotionnel pour TV dans les cinémas (Mauclerc St Aubin du Cormier)

Permet l'affichage du nombres de places vendu pour les scéances à venir.

Fonctionne sur des raspberry  PI sous wheezy/jessie en python 2.7/3

cinepromo.py : gère l'affichage sur l'ecran ou TV
web.py: permet de créer le fichier de configuration config.json avec une interface web (monadresseip:8000)