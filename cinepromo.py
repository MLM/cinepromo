#!/usr/bin/python
# -*- coding: utf-8 -*-
####################################################################################
# File name :cinepromo.py
# Version : 0.5
# Purpose : Affichage Promotion cinema Mauclerc St Aubin du Cormier
# Usages : see README
# Date : 20/02/2020
# Author : mikael.lemasson@orange.fr
# Python : 2.7
# License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
# How To Run : sudo python cinepromo.py
#####################################################################################
####################################################################################
# File name :cinepromo.py
# Version : 0.4
# Purpose : Affichage Promotion cinema Mauclerc St Aubin du Cormier
# Usages : see README
# Date : 26/11/2015
# Author : mikael.lemasson@orange.fr
# Python : 2.7
# License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
# How To Run : sudo python cinepromo.py
#####################################################################################
####################################################################################
# File name :cinepromo.py
# Version : 0.3
# Purpose : Affichage Promotion cinema Mauclerc St Aubin du Cormier
# Usages : see README
# Date : 11/11/2015
# Author : vincent@cinemamauclerc.fr
# Python : 2.7
# License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
# How To Run : sudo python cinepromo.py
#####################################################################################
import os
import sys
import time
import json
import signal
import threading
from xml.etree import ElementTree
import io
#from subprocess import Popen, PIPE, STDOUT
from subprocess import Popen, PIPE
import requests
import pygame
try:
    # Python2
    from urllib2 import urlopen
except ImportError:
    # Python3
    from urllib.request import urlopen

JSON = 'config.json'

Mescouleurs = {"ROUGE" : (237, 0, 0), "VIOLET": (128, 0, 128)}

class mediaplayer:
    screen = None
    screen_height = 0
    screen_width = 0
    select = 0
    num_annonce = 0
    num_diaporama = 0
    ratio = 1
    list_BA = []
    ratioba = 1
    num_BA = 0

    def __init__(self):
        os.putenv('SDL_FBDEV', '/dev/fb0')
        os.putenv('SDL_VIDEODRIVER', 'fbcon')
        os.putenv('SDL_NOMOUSE', '1')
        try:
            pygame.display.init()
        except:
            print("Init : impossible d'initialiser le framebuffer")
            sys.exit(1)

        self.screen_height = pygame.display.Info().current_h
        self.screen_width = pygame.display.Info().current_w
        print(self.screen_width)
        print(self.screen_height)
        if self.screen_width == 1600:
            self.ratio = 0,83
        elif self.screen_width != 1920:
            print("Affichage 1920x1080 impossible")
            sys.exit(1)

        self.screen = pygame.display.set_mode([self.screen_width, self.screen_height], pygame.FULLSCREEN)
        self.screen.fill([0, 0, 0])

        pygame.font.init()
        pygame.mouse.set_visible(False)
        pygame.display.update()

    def __del__(self):
        pass

    def kill_prog(self, signal, frame):
        print("Fermeture du programme")
        subprocess.call(['/usr/bin/killall', 'cec-client'], shell=False)
        subprocess.call(['/usr/bin/killall', 'omxplayer'], shell=False)
        subprocess.call(['/usr/bin/killall', 'omxplayer.bin'], shell=False)
        sys.exit(0)

    def refresh_prog(self, signal, frame):
        self.refreshFlag = 1
        ev = pygame.event.Event(pygame.USEREVENT, {'command': 'refresh'})
        pygame.event.post(ev)
        print("Signal de refresh: Refresh du programme")

    def showImage(self, filename):
        img = pygame.image.load(filename)

        img_height = img.get_height()
        img_width = img.get_width()

        if ((img_height != self.screen_height) or (img_width != self.screen_width)):
            scaled_height = int((float(self.screen_width) / img_width) * img_height)

            if scaled_height > self.screen_height:
                scaled_height = self.screen_height
                scaled_width = int((float(self.screen_height) / img_height) * img_width)
            else:
                scaled_width = self.screen_width

            img_bitsize = img.get_bitsize()

            if (img_bitsize == 24 or img_bitsize == 32):
                img = pygame.transform.smoothscale(img, [scaled_width, (scaled_height-125)])
            else:
                img = pygame.transform.scale(img, [scaled_width, (scaled_height-125)])

            display_x = (self.screen_width - scaled_width) / 2
            display_y = (self.screen_height - scaled_height) / 2
        else:
            img = pygame.transform.smoothscale(img, [1920, (955)])
            display_x = 0
            display_y = 0

        pygame.draw.rect(self.screen, (0, 0, 0), (0, 0, 1920, 955))
        self.screen.blit(img, [display_x, display_y])
        pygame.display.update()

    def showVideo(self, filename):
        pygame.draw.rect(self.screen, (0, 0, 0), (0, 0, 1920, 955))
#        subprocess.call(['/usr/bin/omxplayer', '-o', 'hdmi', filename], shell=False)
        subprocess.call(['/usr/bin/omxplayer', '-o', 'hdmi', filename, '--win', '0 0 1920 1080'], shell=False)
        subprocess.call(['/usr/bin/killall', 'omxplayer'], shell=False)

    def showText(self, my_data, salles):
        heure = time.strftime('%H:%M', time.localtime())
        date = time.strftime('%d/%m/%y', time.localtime())
        pygame.draw.rect(self.screen, (0, 0, 0), (0, 955, 1920, 110))

        font = pygame.font.SysFont('Arial', 40)
        font.set_bold(True)
        heureaff = font.render(heure, 1, (255, 255, 255))
        font.set_bold(True)
        dateaff = font.render(date, 1, (255, 255, 255))
        self.screen.blit(heureaff, (80, 960))
        self.screen.blit(dateaff, (50, 1000))

        for salle in salles:
            print(salles[salle][0])
            print(my_data['PlaceDispo']['salle'][salle]['positionx'])
            print(my_data['PlaceDispo']['salle'][salle]['positiony'])
            print(my_data['PlaceDispo']['salle'][salle]['couleur'])
            font = pygame.font.SysFont('Arial', 45)
            font.set_bold(False)
            textaff = font.render(salles[salle][0], 1, Mescouleurs[my_data['PlaceDispo']['salle'][salle]['couleur']])
            self.screen.blit(textaff, (int(my_data['PlaceDispo']['salle'][salle]['positionx']), int(my_data['PlaceDispo']['salle'][salle]['positiony'])))

        pygame.display.update()

    def boucle_ba(self, my_data=[]):
        print('Boucle BA')
        # print(json.dumps(self.list_BA))
        if self.num_BA+1 == len(self.list_BA):
            self.num_BA = 0
        else:
            self.num_BA += 1
        # print(self.list_BA[self.num_BA])
        pygame.time.set_timer(pygame.USEREVENT+1, 0)
        currentVideo =  my_data['PlaceDispo']['urlba']+self.list_BA[self.num_BA]
        print(currentVideo)
        try:
            player.showVideo(currentVideo)
        except IOError:
            print("boucle_principal: Impossible d'ouvrir la video"), currentVideo
        ev = pygame.event.Event(pygame.USEREVENT+1, {'code': 0})
        pygame.event.post(ev)


    def boucle_principal(self, my_data=[]):
        while my_data['rows'][self.num_annonce]['active'] == 'non':
            if self.num_annonce+1 == len(my_data['rows']):
                self.num_annonce = 0
            else:
                self.num_annonce += 1
        print(my_data['rows'][self.num_annonce])

        if my_data['rows'][self.num_annonce]['type'] == 'image':
            time_pause = int(my_data['rows'][self.num_annonce]['duree'])*1000
            image_url = my_data['rows'][self.num_annonce]['url']
            try:
                image_str = urlopen(image_url).read()
            # create a file object (stream)
                image_file = io.BytesIO(image_str)
                player.showImage(image_file)
            except IOError:
                print("boucle_principal: Impossible d'ouvrir l'url:"), image_url
            pygame.time.set_timer(pygame.USEREVENT+1, time_pause)

        if my_data['rows'][self.num_annonce]['type'] == 'video':
            pygame.time.set_timer(pygame.USEREVENT+1, 0)
            currentVideo = my_data['rows'][self.num_annonce]['url']
            try:
                player.showVideo(currentVideo)
            except IOError:
                print("boucle_principal: Impossible d'ouvrir la video"), currentVideo
            ev = pygame.event.Event(pygame.USEREVENT+1, {'code': 0})
            pygame.event.post(ev)

        if self.num_annonce+1 == len(my_data['rows']):
            self.num_annonce = 0
        else:
            self.num_annonce += 1

    def boucle_bandeau(self, my_data=[]):
        time_pause = int(my_data['PlaceDispo']['duree'])*300
        if my_data['PlaceDispo']['token'] == 'xml':
            urlmedia = my_data['PlaceDispo']['url']
            print(urlmedia)
            rfilms = requests.get(url=urlmedia, stream=True)
            rfilms.raw.decode_content = True

            events = ElementTree.iterparse(rfilms.raw)
            parsed_json_films = {}
            films = []
            sceance = {}
            nbsalle = 0
            salles = {}
            try:
                for event, elem in events:
#                   print(elem.tag, elem.text)
                    if elem.tag == 'AuditoriumNumber':
                        sceance = {}
                        sceance['AuditoriumNumber'] = elem.text
                    if elem.tag == 'ShowStart':
                        sceance['ShowStart'] = elem.text
                    if elem.tag == 'ShowEnd':
                        sceance['ShowEnd'] = elem.text
                    if elem.tag == 'ShowId':
                        sceance['ShowId'] = elem.text
                    if elem.tag == 'FilmTitle':
                        sceance['FilmTitle'] = elem.text
                    if elem.tag == 'FilmDepth':
                        sceance['FilmDepth'] = elem.text
                    if elem.tag == 'FilmVersion':
                        if elem.text == 'VERSION_LOCAL':
                            sceance['FilmVersion'] = 'VF'
                        else:
                            sceance['FilmVersion'] = 'VO'
                    if elem.tag == 'FilmAllocineId':
                        sceance['FilmAllocineId'] = elem.text
                    if elem.tag == 'AuditoriumCapacity':
                        sceance['AuditoriumCapacity'] = elem.text
                    if elem.tag == 'SpectatorsNumber':
                        sceance['SpectatorsNumber'] = elem.text
                        films.append(sceance)
 #               print(films)
                self.list_BA = []
                for film in films:
#                   print(film)
                    try:
                        dateseance = film['ShowStart'].split("T")
                        heureseance = dateseance[1].split(":")
                        dateseance = dateseance[0].split("-")
#                        print(dateseance)
#                        print(heureseance)

                        #calcul nombre places restantes
                        nbplaces = int(film['AuditoriumCapacity']) - int(film['SpectatorsNumber'])

                        t = (int(dateseance[0]), int(dateseance[1]), int(dateseance[2]), int(heureseance[0]), int(heureseance[1]), 0, 0, 0, 0)
                        seance_time_epoch = time.mktime(t)
                        localt = time.time()
#                       print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(localt)))
#                       print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(seance_time_epoch)))
                        if (int(film['AuditoriumNumber']) != nbsalle and int(time.time()) < seance_time_epoch+int(my_data['PlaceDispo']['dureeapresdebutsceance'])):
                            film['FilmTitlesuite'] = ""
                            salles[film['AuditoriumNumber']] = ["Salle "+film['AuditoriumNumber']+": "+film['FilmTitlesuite']+" - "+film['FilmTitle'][:25]+" "+film['FilmVersion']+" "+film['FilmDepth']+" - places restantes: "+str(nbplaces)+"/"+film['AuditoriumCapacity']]
                            nbsalle += 1
                        if nbsalle == int(my_data['PlaceDispo']['nbsalle']):
                            break
                    except:
                        print("erreur syntaxe fichier source "+urlmedia)
 #               print(salles)
                    self.list_BA.append(film['FilmAllocineId'])
                player.showText(my_data, salles)
            except:
                print("erreur fichier source: " + urlmedia)

        else:
            urlmedia = my_data['PlaceDispo']['url']+"media?api_token="+my_data['PlaceDispo']['token']
            # sending get request and saving the response as response object
            print(urlmedia)
            rfilms = requests.get(url=urlmedia)
            # extracting data in json format
            parsed_json_films = rfilms.json()

            # CREATE LISTE FILMS NAME + INDEX FILMS
            film = []

            for shows in parsed_json_films:
                film.append(shows['id'])
                film.append(shows["title"])

            try:
                placedispo = film.append
                salles = {}
                nbsalle = 0
                for ligne in placedispo:
                    try:
                        if ligne == '#':
                            break
                        champs = ligne.replace('"', '').split("#")
                        dateseance = champs[2].split("/")
                        heureseance = champs[3].split(":")

                        if champs[10] == "1":
                            champs[10] = "VF"
                        if champs[10] == "2":
                            champs[10] = "VO"
                        if champs[13] == "NUM":
                            champs[13] = ""
                        if champs[13] == "N3D":
                            champs[13] = "3D"
                        #calcul nombre places restantes
                        champs[8] = str(int(champs[6])-int(champs[7]))

                        t = (int(dateseance[2]), int(dateseance[1]), int(dateseance[0]), int(heureseance[0]), int(heureseance[1]), 0, 0, 0, 0)
                        seance_time_epoch = time.mktime(t)
                        localt = time.time()
                        #print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(localt))
                        #print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(seance_time_epoch))
                        if int(time.time()) < seance_time_epoch+int(my_data['PlaceDispo']['dureeapresdebutsceance']):
                            salles[champs[1]] = ["Salle "+champs[1]+": "+champs[3]+" - "+champs[4][:25]+" "+champs[10]+" "+champs[13]+" - places restantes: "+champs[8]+"/"+champs[6]]
                            nbsalle += 1
                        if nbsalle == int(my_data['PlaceDispo']['nbsalle']):
                            break
                    except:
                        print("erreur syntaxe fichier source " + urlmedia)
#                print (salles)
                player.showText(my_data, salles)
            except:
                print("erreur fichier source: "+urlmedia)
    #            player.showText( "IMPOSSIBLE D'ACCEDER AUX DONNEES", "" , "")
        pygame.time.set_timer(pygame.USEREVENT+2, time_pause)

    def commande(self, p):
        for line in iter(p.stdout.readline, ''):
            if b'key pressed: select' in line:
                print('select')
                ev = pygame.event.Event(pygame.USEREVENT, {'command': 'refresh'})
                pygame.event.post(ev)
                pygame.event.post(ev)
        p.stdout.close()

    def boucle_client(self, my_data=[]):
        try:
            taille_max = len(my_data['diaporama']) - 1
            if taille_max > 0:
                pygame.time.set_timer(pygame.USEREVENT+4, (int(data['config']['temps_diapo'])*60*1000))
                player.showText('defaut', '')
                clientPhoto = PHOTO_CL_PATH + my_data['diaporama'][self.num_diaporama]['url']
                self.showImage(clientPhoto)
            else:
                print('pas de diapo')
                player.select = 0
                ev = pygame.event.Event(pygame.USEREVENT, {'command': 'refresh'})
                pygame.event.post(ev)
        except:
            pygame.time.set_timer(pygame.USEREVENT+4, (30*1000))
            pygame.draw.rect(self.screen, (255, 255, 255), (0, 0, 1920, 980))
            img = pygame.image.load('imgdef.jpg')
            self.screen.blit(img, (200, 0))
            pygame.display.update()

    def void(self):
        return True

def dont_quit(signal, frame):
    print('Catch signal: {}'.format(signal))

# ///////////////////////////////////////////// Main ///////////////////////////////////////////// #
if __name__ == '__main__':
    signal.signal(signal.SIGHUP, dont_quit)
    player = mediaplayer()

#   gestion des signaux -9
    signal.signal(signal.SIGINT, player.kill_prog)
    signal.signal(signal.SIGHUP, player.refresh_prog)

    cmd = "cec-client"
    p = Popen(cmd, stdout=PIPE, stdin=PIPE, shell=True)
    proc_c = threading.Thread(target=player.commande, args=(p,))
    proc_c.daemon = True
    proc_c.start()

#  envoie du start
    ev = pygame.event.Event(pygame.USEREVENT, {'command': 'refresh'})
    pygame.event.post(ev)

    while True:
        event = pygame.event.wait()
        print(event)
        if event.type == pygame.USEREVENT:
            if event.command == 'refresh':
                print('refresh')
                try:
                    data = json.loads(open(JSON).read())
                except IOError:
                    data = {"rows": [{"type": "image", "url": "http://ds.cinemamauclerc.fr/cinempromo/demonstration.png", "duree": "60"}]}
                player.boucle_principal(data)
                player.boucle_bandeau(data)

        elif event.type == pygame.USEREVENT+1:
            print('Publicite')
            print(player.num_annonce)
            print(player.ratioba)
            print(data['PlaceDispo']['ratioba'])
            if(player.ratioba == int(data['PlaceDispo']['ratioba']) and len(player.list_BA) > 0):
                player.boucle_ba(data)
                player.ratioba = 1
            else:
                player.boucle_principal(data)
                player.ratioba = player.ratioba+1

        elif event.type == pygame.USEREVENT+2:
            print('bandeau')
            player.boucle_bandeau(data)

        #p.stdin.write("as\n")
        #p.stdin.write("on 0\n")
